extern bezier_bernstein_polynomial

section .data
ten: dq 10.0
two: dq 2.0
one: dq 1.0
zero: dq 0.0

section .text
global bezier_bernstein

;;; Argumenty:
;;; rdi - wskaźnik na pierwszy wynikowy punkt
;;; esi - maksymalna liczba punktów do wygenerowania
;;; rdx - wskaźnik na pierwszy punkt kontrolny
;;; ecx - liczba punktów kontrolnych

;;; r8 - czy nie jest obliczany pierwszy punkt
;;; r9 - wskaźnik na aktualny punkt
;;; r10 - wskaźnik na punkt po ostatnim możliwym (warunek zakończenia)
;;; r11 - wskaźnik na aktualny punkt kontrolny
;;; r12 - wskaźnik na punkt kontrolny po ostatnim możliwym (warunek zakończenia)

;;; xmm1 - t
;;; xmm2 - wskazane dt
;;; xmm3 - aktualne dt

bezier_bernstein:
    push rbp
    mov rbp, rsp

    sub rsp, 104

    mov r8, 0                   ; Nie pierwszy punkt
    mov r9, rdi                 ; Pierwszy obliczany punkt
    ;; Punkt po ostatnim dozwolonym.
    movsx r10, esi
    shl r10, 4
    add r10, rdi
    ;; Punkt kontrolny po ostatnim dozwolonym.
    movsx r12, ecx
    shl r12, 4
    add r12, rdx
    ;; liczba punktów kontrolnych - 1
    dec ecx
    ;; liczba punktów - 1
    movsx rsi, esi
    dec rsi
    cvtsi2sd xmm0, rsi
    movsd xmm2, [ten]
    ;; Wskazane dt.
    divsd xmm2, xmm0            ; 10 / (liczba punktów - 1)
    ;; Aktualne dt.
    movsd xmm3, xmm2
    ;; Aktualne t.
    movsd xmm1, [zero]

    ;; Pętla po punktach.
.point_loop:
    ;; Początek pętli po punktach kontrolnych.
    ;; xmm4 - współrzędna x punktu
    ;; xmm5 - współrzędna y punktu
    ;; Wyzeruj współrzędne aktualnego punktu.
    movsd xmm4, [zero]
    movsd xmm5, [zero]
    ;; Pierwszy punkt kontrolny.
    mov r11, rdx
.control_point_loop:
    ;; Przygotowanie do wywołania funkcji obliczającej wartość wielomianu.
    ;; Kopia używanych rejestrów.
    mov [rbp - 8], ecx
    mov [rbp - 16], rdx
    mov [rbp - 24], rdi
    mov [rbp - 32], r8
    mov [rbp - 40], r9
    mov [rbp - 48], r10
    mov [rbp - 56], r11
    mov [rbp - 64], r12
    movsd [rbp - 72], xmm1
    movsd [rbp - 80], xmm2
    movsd [rbp - 88], xmm3
    movsd [rbp - 96], xmm4
    movsd [rbp - 104], xmm5
    ;; Liczba punktów kontrolnych - 1.
    mov edi, ecx
    ;; Numer aktualnego punktu kontrolnego.
    mov rsi, r11
    sub rsi, rdx
    shr esi, 4                  ; / 16
    ;; t
    movsd xmm0, xmm1
    call bezier_bernstein_polynomial
    ;; Odtwórz zachowane rejestry.
    mov ecx, [rbp - 8]
    mov rdx, [rbp - 16]
    mov rdi, [rbp - 24]
    mov r8, [rbp - 32]
    mov r9, [rbp - 40]
    mov r10, [rbp - 48]
    mov r11, [rbp - 56]
    mov r12, [rbp - 64]
    movsd xmm1, [rbp - 72]
    movsd xmm2, [rbp - 80]
    movsd xmm3, [rbp - 88]
    movsd xmm4, [rbp - 96]
    movsd xmm5, [rbp - 104]

    ;; Wynik w xmm0.
    ;; xmm6 - współrzędna x/y punktu kontrolnego.
    ;; x
    movsd xmm6, [r11]           ; x punktu kontrolnego
    mulsd xmm6, xmm0            ; x * wartość wielomianu
    addsd xmm4, xmm6            ; Dodaj do współrzędnej x obliczanego punktu
    ;; y
    movsd xmm6, [r11 + 8]
    mulsd xmm6, xmm0
    addsd xmm5, xmm6

    ;; Rozpatrz kolejny punkt kontrolny.
    add r11, 16
    ;; Po ostatnim punkcie kontrolnym?
    cmp r11, r12
    jne .control_point_loop

.check_generated_point:
    ;; Sprawdź odległość między obliczonym punktem a poprzednim.
    ;; Nie pierwszy punkt?
    test r8, r8
    jz .save_generated_point    ; Skocz jeśli r8 = 0
    ;; Musimy sprawdzić odległość.
    ;; xmm6,7 - współrzędna poprzedniego punktu, różnica kwadratów
    ;; xmm6 - suma różnicy kwadratów
    ;; x
    movsd xmm6, xmm4            ; x
    subsd xmm6, [r9 - 16]       ; x - poprzedni x
    mulsd xmm6, xmm6            ; Δx ^ 2
    ;; y
    movsd xmm7, xmm5
    subsd xmm7, [r9 - 8]
    mulsd xmm7, xmm7
    ;; Δx^2 + Δy^2
    addsd xmm6, xmm7
    ;; Δx^2 + Δy^2 <= 1 ?
    ucomisd xmm6, [one]
    jbe .save_generated_point   ; <= 1
    divsd xmm3, [two]           ; Używane dt / 2
    subsd xmm1, xmm3            ; t - dt (/ 2)
    jmp .point_loop
.save_generated_point:
    ;; Zapisz obliczone współrzędne punktu w pamięci.
    movsd [r9], xmm4
    movsd [r9 + 8], xmm5
    ;; Kolejny punkt.
    add r9, 16
    ;; Przywróć wskazane dt.
    movsd xmm3, xmm2
    ;; t + dt
    addsd xmm1, xmm2
    ;; Jeden punkt jest już na pewno załatwiony.
    mov r8, 1

    ;; Sprawdzenie może być na końcu pętli, bo co najmniej jeden punkt będzie
    ;; do wygenerowania.
    ;; Ostatni punkt został obliczony?
    cmp r9, r10
    je .end
    ;; t <= 1?
    ucomisd xmm1, [one]
    jbe .point_loop

.end:
    ;; Liczba wygenerowanych punktów.
    mov rax, r9                 ; Wskaźnik na punkt po ostatnim wygenerowanym
    sub rax, rdi                ; - wskaźnik na pierwszy punkt
    shr rax, 4                  ; / 16

    mov rsp, rbp
    pop rbp
    ret
