#include <stdio.h>
#include <math.h>                /* abs */

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include "bezier.h"

#define MAX_CONTROL_POINTS 20
#define MIN_CONTROL_POINTS 2
#define MAX_POINTS 5000

#define FPS 1
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
/* Zakres punktów kontrolnych. */
#define CONTROL_POINT_RANGE_X (INFO_OFFSET_X - 10)
#define CONTROL_POINT_RANGE_Y WINDOW_HEIGHT
/* Lokacja informacji.  */
#define INFO_OFFSET_X WINDOW_WIDTH - 200
#define INFO_OFFSET_Y 50
/* Pozycja punktów kontrolnych. */
#define INFO_OFFSET_Y_CTL_POINTS INFO_OFFSET_Y
#define INFO_OFFSET_X_CTL_POINT_X (INFO_OFFSET_X + 5)
#define INFO_OFFSET_X_CTL_POINT_Y (INFO_OFFSET_X_CTL_POINT_X + 80)
#define OFFSET_BETWEEN_LINES 25
#define FONT_SIZE 20
#define LINE_THICKNESS 1
#define CIRCLE_R 3
#define CLICK_TOLERATE 5

static ALLEGRO_COLOR background_color;
static ALLEGRO_COLOR bezier_line_color;
static ALLEGRO_COLOR control_line_color;
static ALLEGRO_COLOR control_point_color;
static ALLEGRO_COLOR white_color;

static void draw_info(const Point *control_points,
                      unsigned num_control_points,
                      char use_berstein,
                      const ALLEGRO_FONT *font)
{
    const Point *cp;
    unsigned ctl_info_y = INFO_OFFSET_Y_CTL_POINTS;
    char buffer[256];

    /* Narysuje krzywą złożoną z punktów kontrolnych i wyróżnij je. */
    for (cp = control_points; cp != control_points + num_control_points - 1; ++cp) {
        al_draw_line(cp->x, cp->y, (cp + 1)->x, (cp + 1)->y,
                     control_line_color, LINE_THICKNESS);
        al_draw_filled_circle(cp->x, cp->y,
                              CIRCLE_R, control_point_color);
    }
    /* Wyróżnij ostatni punkt kontrolny. */
    al_draw_filled_circle(control_points[num_control_points - 1].x,
                          control_points[num_control_points - 1].y,
                          CIRCLE_R,
                          control_point_color);

    /* Oddziel informację od krzywej. */
    al_draw_line(INFO_OFFSET_X, 0, INFO_OFFSET_X, WINDOW_HEIGHT,
                 white_color, 3 * LINE_THICKNESS);
    /* Informacja o algorytmie użytym do wygenerowania krzywej. */
    al_draw_text(font, white_color, INFO_OFFSET_X_CTL_POINT_X, 0,
                 ALLEGRO_ALIGN_LEFT, use_berstein ? "Bernstein" : "Casteljau");
    /* Informacja o punktach kontrolnych. */
    for (cp = control_points; cp != control_points + num_control_points; ++cp) {
        sprintf(buffer, "X: %d", (int) cp->x);
        al_draw_text(font, white_color,
                     INFO_OFFSET_X_CTL_POINT_X, ctl_info_y,
                     ALLEGRO_ALIGN_LEFT, buffer);
        sprintf(buffer, "Y: %d", (int) cp->y);
        al_draw_text(font, white_color,
                     INFO_OFFSET_X_CTL_POINT_Y, ctl_info_y,
                     ALLEGRO_ALIGN_LEFT, buffer);
        /* Kolejne współrzędne niżej o 25 pikseli. */
        ctl_info_y += OFFSET_BETWEEN_LINES;
    }
}

void draw_bezier_curve(Point *points,
                       unsigned *num_generated_points,
                       const Point *control_points,
                       unsigned num_control_points,
                       char generate,
                       char use_berstein)
{
    Point *p;

    if (generate) {
        if (use_berstein) {
            *num_generated_points = bezier_bernstein(points,
                                                     MAX_POINTS,
                                                     control_points,
                                                     num_control_points);
        } else {
            *num_generated_points = bezier_casteljau(points,
                                                     MAX_POINTS,
                                                     control_points,
                                                     num_control_points);
        }
    }

    /* Narysuj krzywą Beziera, korzystając z wygenerowanych punktów.
     * Ale nie łącz ich!
     *
     * Aby czas rysowania był rozsądny korzystając z funkcji `al_put_pixel'
     * bitmapa musi być zablokowana.
     */
    for (p = points; p != points + *num_generated_points; ++p) {
        al_put_pixel(p->x + 0.5, p->y + 0.5, bezier_line_color);
    }
}

void redraw_display(Point *points,
                    unsigned *num_generated_points,
                    const Point *control_points,
                    unsigned num_control_points,
                    char generate,
                    char use_berstein,
                    ALLEGRO_DISPLAY *display,
                    ALLEGRO_FONT *font)
{
    ALLEGRO_BITMAP *bitmap = al_get_backbuffer(display);

    /* Załóż blokadę, by przyspieszyć operację `put_pixel'. */
    al_clear_to_color(background_color);
    draw_info(control_points, num_control_points, use_berstein, font);
    al_lock_bitmap(bitmap,
                   al_get_bitmap_format(bitmap),
                   ALLEGRO_LOCK_READWRITE);
    draw_bezier_curve(points, num_generated_points,
                      control_points, num_control_points,
                      generate, use_berstein);
    al_unlock_bitmap(bitmap);
    al_flip_display();
}

void handle_click_on_control_point(Point *control_points,
                                   unsigned num_control_points,
                                   const ALLEGRO_EVENT *ev,
                                   Point **chosen_control_point)
{
    if (*chosen_control_point) {
        /* Jeśli jakiś punkt kontrolny został wcześniej wybrany, to sprawdź
         * czy aktualny wybór jest ok. */
        if (ev->mouse.x <= INFO_OFFSET_X) {
            (*chosen_control_point)->x = ev->mouse.x;
            (*chosen_control_point)->y = ev->mouse.y;
        }
        /* Zapomnij o punkcie i przygotuj się do obsługi kolejnego. */
        *chosen_control_point = NULL;
    } else {
        /* Sprawdź czy użytkownik trafił na jakiś punkt.
         * Może pomylić się o ok. 3 piksele.
         */
        Point *cp;

        for (cp = control_points;
             cp != control_points + num_control_points;
             ++cp) {
            if (abs(ev->mouse.x - cp->x) <= CLICK_TOLERATE &&
                abs(ev->mouse.y - cp->y) <= CLICK_TOLERATE) {
                *chosen_control_point = cp;
                break;
            }
        }
    }
}

int main(void)
{
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_TIMER *timer = NULL;
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;
    ALLEGRO_FONT *font = NULL;
    char done = 0, redraw = 0, generate = 1, use_berstein = 1;
    Point *chosen_control_point = NULL;
    Point control_points[MAX_CONTROL_POINTS] = {
        { 50, 30 },
        { 200, 550 },
        { 300, 500 },
        { 350, 300 },
        { 550, 234 }
    };
    unsigned num_control_points = 5;
    Point points[MAX_POINTS];
    unsigned num_generated_points = 0;

    srand(time(NULL));

    /* inicjalizacja allegro. */
    if (!al_init() ||
        !al_install_keyboard() ||
        !al_install_mouse() ||
        !al_init_primitives_addon()) {
        exit(1);
    }
    al_init_font_addon();
    al_init_ttf_addon();

    /* Używane kolory. */
    background_color = al_map_rgb(0, 0, 0);
    bezier_line_color = al_map_rgb(255, 0, 0);
    control_line_color = al_map_rgb(0, 255, 0);
    control_point_color = al_map_rgb(120, 255, 0);
    white_color = al_map_rgb(255, 255, 255);

    display = al_create_display(WINDOW_WIDTH, WINDOW_HEIGHT);
    al_clear_to_color(background_color);

    timer = al_create_timer(1.0 / FPS);

    event_queue = al_create_event_queue();
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    al_register_event_source(event_queue, al_get_mouse_event_source());

    font = al_load_ttf_font("DejaVuSans.ttf", FONT_SIZE, ALLEGRO_TTF_NO_KERNING);

    redraw_display(points, &num_generated_points,
                   control_points, num_control_points,
                   generate, use_berstein, display, font);
    al_start_timer(timer);
    while (!done) {
        ALLEGRO_EVENT ev;

        al_wait_for_event(event_queue, &ev);
        switch (ev.type) {
        case ALLEGRO_EVENT_DISPLAY_CLOSE:
            done = 1;
            break;
        case ALLEGRO_EVENT_TIMER:
            redraw = 1;
            break;
        case ALLEGRO_EVENT_KEY_UP:
            switch (ev.keyboard.keycode) {
            case ALLEGRO_KEY_T:
                use_berstein = !use_berstein;
                generate = 1;
                break;
            case ALLEGRO_KEY_EQUALS:
                /* Sprawdź czy znak jest plusem (równa się też jest ok). */
                if (num_control_points < MAX_CONTROL_POINTS) {
                    /* Dodaj losowy punkt kontrolny. */
                    control_points[num_control_points].x =  \
                        rand() % CONTROL_POINT_RANGE_X;
                    control_points[num_control_points].y =  \
                        rand() % CONTROL_POINT_RANGE_Y;
                    ++num_control_points;
                    generate = 1;
                } else {
                    fprintf(stderr, "Zbyt dużo punktów kontrolnych\n");
                }
                break;
            case ALLEGRO_KEY_MINUS:
                if (num_control_points > MIN_CONTROL_POINTS) {
                    --num_control_points;
                    generate = 1;
                } else {
                    fprintf(stderr, "Zbyt mało punktów kontrolnych\n");
                }
                break;
            }
            break;
        case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
            if (ev.mouse.x < INFO_OFFSET_X) {
                /* Być może kliknięcie na punkt kontrolny.
                 * Jeśli tak to za kolejnym razem może zmień położenie punktu
                 * kontrolnego.
                 */
                handle_click_on_control_point(control_points,
                                              num_control_points,
                                              &ev,
                                              &chosen_control_point);
                /* Odśwież na wszelki wypadek. */
                generate = 1;
            }
            break;
        }

        if ((redraw || generate) && al_is_event_queue_empty(event_queue)) {
            redraw_display(points, &num_generated_points,
                           control_points, num_control_points,
                           generate, use_berstein, display, font);
            redraw = generate = 0;
        }
    }

    /* Czyszczenie. */
    al_destroy_event_queue(event_queue);
    al_destroy_timer(timer);
    al_destroy_display(display);

    return 0;
}
