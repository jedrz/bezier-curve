#include <stdlib.h>
#include <string.h>             /* memcpy */
#include <math.h>

#include "bezier.h"

static unsigned bezier_newton(unsigned n, unsigned k)
{
    static unsigned cache[NEWTON_MAX];
    unsigned index;

    if (k == 0 || k == n) {
        return 1;
    }

    index = n * (n + 1) / 2 + k;
    if (!cache[index]) {
        cache[index] = bezier_newton(n - 1, k - 1) + bezier_newton(n - 1, k);
    }
    return cache[index];
}

double bezier_bernstein_polynomial(unsigned n, unsigned i, double t)
{
    return bezier_newton(n, i) * pow(t, i) * pow(1 - t, n - i);
}

static Point *bezier_bernstein_generate_valid_point(Point *p,
                                                    Point *p_end,
                                                    double t,
                                                    double dt,
                                                    double first_dt,
                                                    char not_first,
                                                    const Point *control_points,
                                                    unsigned num_control_points)
{
    const Point *ctl_p;

    p->x = p->y = 0;
    for (ctl_p = control_points;
         ctl_p != control_points + num_control_points;
         ++ctl_p) {
        double berstein_val = bezier_bernstein_polynomial(num_control_points - 1,
                                                          ctl_p - control_points,
                                                          t);
        p->x += ctl_p->x * berstein_val;
        p->y += ctl_p->y * berstein_val;
    }
    if (not_first && pow(p->x - (p - 1)->x, 2) + pow(p->y - (p - 1)->y, 2) > 1) {
        dt /= 2;
        /* Wygeneruj trochę 'wcześniejszy' punkt. */
        return bezier_bernstein_generate_valid_point(p,
                                                     p_end,
                                                     t - dt,
                                                     dt,
                                                     first_dt,
                                                     1,
                                                     control_points,
                                                     num_control_points);
    } else if (p + 1 != p_end && t + first_dt <= 1) {
        return bezier_bernstein_generate_valid_point(p + 1,
                                                     p_end,
                                                     t + first_dt,
                                                     first_dt,
                                                     first_dt,
                                                     1,
                                                     control_points,
                                                     num_control_points);
    }
    /* Adres ostatniego wygenerowanego punktu. */
    return p;
}

/* Wersja z łączeniem punktów tzn. sprawdza odległość między nowo
 * wygenerowanym punktem a poprzednim, czy jest mniejsza od jedności.
 * Tak naprawdę odległość może być większa (sqrt(2)).
 */
unsigned bezier_bernstein_c(Point *points, unsigned max_num_points,
                            const Point *control_points, unsigned num_control_points)
{
    Point *p = bezier_bernstein_generate_valid_point(points,
                                                     points + max_num_points,
                                                     0,
                                                     10.0 / (max_num_points - 1),
                                                     10.0 / (max_num_points - 1),
                                                     0,
                                                     control_points,
                                                     num_control_points);

    /* Liczba wygenerowanych punktów. */
    return p - points + 1;
}

/* Wersja iteracyjna powyższej funkcji. */
unsigned bezier_bernstein_cc(Point *points, unsigned max_num_points,
                             const Point *control_points, unsigned num_control_points)
{
    Point *p = points;
    double initial_dt = 10.0 / (max_num_points - 1);
    double dt = initial_dt;
    double t = 0;
    char not_first = 0;

    while (p != points + max_num_points && t <= 1) {
        const Point *ctl_p;

        p->x = p->y = 0;
        for (ctl_p = control_points;
             ctl_p != control_points + num_control_points;
             ++ctl_p) {
            double berstein_val = bezier_bernstein_polynomial(
                num_control_points - 1,
                ctl_p - control_points,
                t);

            p->x += ctl_p->x * berstein_val;
            p->y += ctl_p->y * berstein_val;
        }

        if (not_first &&
            pow(p->x - (p - 1)->x, 2) + pow(p->y - (p - 1)->y, 2) > 1) {
            /* Wygeneruj trochę wcześniejszy punkt. */
            dt /= 2;
            t -= dt;
        } else {
            ++p;
            t += (dt = initial_dt);
            not_first = 1;
        }
    }

    /* Liczba wygenerowanych punktów. */
    return p - points;
}

/*
unsigned bezier_bernstein(Point *points, unsigned max_num_points,
                          const Point *control_points, unsigned num_control_points)
{
    return bezier_bernstein_cc(points, max_num_points, control_points,
                               num_control_points);
}
*/

/* http://en.wikipedia.org/wiki/De_Casteljau's_algorithm  */
unsigned bezier_casteljau(Point *points, unsigned num_points,
                          const Point *control_points, unsigned num_control_points)
{
    Point *p, *tmp_points = malloc(sizeof(*tmp_points) * num_control_points);
    double dt = 1.0 / (num_points - 1);

    for (p = points; p != points + num_points; ++p) {
        double t = dt * (p - points);
        unsigned i;

        /* Pierwszy krok - przybliż korzystając tylko z punktów kontrolnych. */
        memcpy(tmp_points, control_points,
               sizeof(*tmp_points) * num_control_points);
        /* Podziel łamaną n - 1 razy, gdzie n to liczba punktów kontrolnych. */
        for (i = 1; i < num_control_points; ++i) {
            Point *tmp_p;

            for (tmp_p = tmp_points;
                 tmp_p != tmp_points + num_control_points - i;
                 ++tmp_p) {
                /* Podziel odcinek na dwie części w stosunku t : (1 - t). */
                tmp_p->x = t * tmp_p->x + (1 - t) * (tmp_p + 1)->x;
                tmp_p->y = t * tmp_p->y + (1 - t) * (tmp_p + 1)->y;
            }
        }
        /* Wynik jest w pierwszym punkcie. */
        p->x = tmp_points->x;
        p->y = tmp_points->y;
    }
    free(tmp_points);

    return num_points;
}
