#ifndef _BEZIER_H_
#define _BEZIER_H_

#define NEWTON_MAX_N 20
#define NEWTON_MAX NEWTON_MAX_N * (NEWTON_MAX_N + 1) / 2 + NEWTON_MAX_N

typedef struct {
    double x, y;
} Point;

double bezier_bernstein_polynomial(unsigned, unsigned, double);
unsigned bezier_bernstein(Point *, unsigned, const Point *, unsigned);
unsigned bezier_casteljau(Point *, unsigned, const Point *, unsigned);

#endif
