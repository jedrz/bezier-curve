CC = gcc
OBJ = main.o bezier.o bezier_nasm.o
LINKOBJ = $(OBJ)
SRC = main.c bezier.c
BIN = bezier
CFLAGS = -Wall -Wextra -Wshadow -pedantic
LFLAGS = -lm -lallegro -lallegro_primitives -lallegro_font -lallegro_ttf

$(BIN): $(OBJ)
	$(CC) $(LFLAGS) $(LINKOBJ) -o $(BIN)

main.o: main.c
	$(CC) $(CFLAGS) -c main.c -o main.o

bezier.o: bezier.c
	$(CC) $(CFLAGS) -c bezier.c -o bezier.o

bezier_nasm.o: bezier_nasm.s
	nasm -f elf64 bezier_nasm.s

clean:
	rm -f *.o $(BIN)
